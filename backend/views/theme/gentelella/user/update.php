<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update User: ' . ' ' . $model->user_id;
$this->title = 'Обновление пользователя: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update padding-md">

    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h2>

            <?= $this->render('_form', [
                'model' => $model,
                'partner' => $partner,
            ]) ?>
        </div>
    </div>

</div>
