<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var common\models\User $model */
/* @var $partner common\models\PartnerInfo */
/* @var $form yii\widgets\ActiveForm */

//$partner->name = $model->username;
//var_dump($partner);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#user" data-toggle="tab">Пользователь</a></li>
        <li><a href="#partner" data-toggle="tab">Партнер</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="user">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'role_id')->dropDownList(\common\models\Role::getRolesList()) ?>
                    <?= $form->field($model, 'status')->dropDownList([1 => 'Включено', 2 => 'Выключено']) ?>
                </div>
                <div class="col-md-3">

                    <div class="form-group" style="width: 180px;">
                        <label class="control-label"><?php echo $model->getAttributeLabel('image'); ?></label>
                        <?php echo InputFile::widget([
                            'language'      => 'ru',
                            'name'          => 'User[image]',
                            'value'         => $model->image,
                            'id'            => 'user-image',
                            'controller'    => 'elfinder',
                            'filter'        => 'image',
                            'template'      => '<div class="thumbnail text-center">'.(($model->image) ? '<img src="'.$model->image.'" id="image-user-image">' : '<i class="fa fa-image" style="font-size: 150px" id="icon-user-image"></i>').'<div class="input-group">{input}<span class="input-group-btn">{button}</span></div></div>',
                            'options'       => ['class' => 'form-control'],
                            'buttonOptions' => ['class' => 'btn btn-default'],
                            'multiple'      => false,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>