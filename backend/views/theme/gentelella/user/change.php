<?php

use \yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $user \common\models\User */

$this->title = 'Смена пароля';
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['user/profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="padding-md">
    <div class="row">
        <div class="col-md-9">
            <div class="partner-profile-password">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Смена пароля</h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                                <div class="alert alert-success">
                                    <i class="fa fa-check-circle"></i> <?php echo Yii::$app->session->getFlash('success'); ?>
                                    <a class="pull-right button-close"><i class="fa fa-times"></i></a>
                                </div>
                            <?php } ?>
                            <?php $form = ActiveForm::begin(); ?>
                            <?php echo $form->field($user, 'old_password')->passwordInput(); ?>
                            <?php echo $form->field($user, 'password')->passwordInput(); ?>
                            <?php echo $form->field($user, 'confirm')->passwordInput(); ?>
                            <?php echo \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>