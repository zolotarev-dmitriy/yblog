<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Image;
use backend\widgets\FileUpload;

/* @var $model \common\models\User */
/* @var $partner \common\models\PartnerInfo */
/* @var $this \yii\web\View */

$this->title = 'Редактировать профиль';

$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="padding-md">
    <div class="row">
        <div class="col-sm-12">
            <p class="lead"><i class="fa fa-edit"></i> <?php echo Html::encode($this->title); ?></p>

            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success">
                    <i class="fa fa-check-circle"></i> <?php echo Yii::$app->session->getFlash('success'); ?>
                    <a class="pull-right button-close"><i class="fa fa-times"></i></a>
                </div>
            <?php } ?>

            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($partner, 'contact_person')->textInput(); ?>
                    <?= $form->field($partner, 'email')->textInput(); ?>
                    <?= $form->field($partner, 'name')->textInput(); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($partner, 'inn')->textInput(); ?>
                    <?= $form->field($partner, 'kpp')->textInput(); ?>
                    <?= $form->field($partner, 'f_address')->textInput(); ?>
                    <?= $form->field($partner, 'y_address')->textInput(); ?>
                </div>
                <div class="col-sm-4">
                    <?php echo FileUpload::widget([
                        'model' => $model,
                        'name' => 'image',
                        'attribute' => 'image',
                        'uploadUrl' => \yii\helpers\Url::to(['user/upload-files']),
                        'buttonUpload' => 'Загрузить',
                        'thumb' => Image::resize($model->image, FileUpload::THUMB_WIDTH, FileUpload::THUMB_HEIGHT)
                    ]) ?>
                </div>
            </div>
            <div class="btn-group">
                <?= Html::a('Вернуться назад', Yii::$app->request->referrer, ['class' => 'btn btn-danger']); ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
