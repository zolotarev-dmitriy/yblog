<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="padding-md">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-street-view"></i> <?= Html::encode($this->title) ?></h2>
            <p>
                <?= Html::a('Обновить', ['user/update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['user/delete', 'id' => $model->user_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user_id',
                    'email:email',
                    'password',
                    'role_id',
                    'created',
                    'updated',
                    'status',
                    //'auth_key',
                    'username',
                ],
            ]) ?>
        </div>
    </div>
</div>
