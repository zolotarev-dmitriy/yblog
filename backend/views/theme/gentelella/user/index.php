<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\theme\gentelella\FormAsset;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

FormAsset::register($this);

?>
<div class="padding-md">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-7">
                    <p class="lead"><i class="fa fa-list"></i> <?= Html::encode($this->title) ?></p>
                </div>
                <div class="col-md-5">
                    <p class="pull-right">
                        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
            </div>

            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success">
                    <i class="fa fa-check-circle"></i> <?php echo Yii::$app->session->getFlash('success'); ?>
                    <a class="pull-right button-close"><i class="fa fa-times"></i></a>
                </div>
            <?php } ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'user_id',
                    'username',
                    'email:email',
                    'role_id' => [
                        'attribute' => 'role_id',
                        'format' => 'html',
                        'value' => function($model) {
                            if ($model->role_id) {
                                return $model->role_id . ': ' . Html::a($model->role->name, ['role/update', 'id' => $model->role_id]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    'created' => [
                        'attribute' => 'created',
                        'value' => function($model) {
                            return date('d.m.Y H:i:s', strtotime($model->created));
                        }
                    ],
                    // 'updated',
                    'status' => [
                        'attribute' => 'status',
                        'value' => function($model) {
                            return \common\components\Status::getStatus($model->status);
                        }
                    ],
                    // 'auth_key',
                    // 'username',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>