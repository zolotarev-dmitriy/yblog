<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Role */

$this->title = 'Добавление роли';
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Роли пользователей', 'url' => ['role/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-create padding-md">
    <div class="row">
        <div class="col-md-12">
        <h2><i class="fa fa-plus-square"></i> <?= Html::encode($this->title) ?></h2>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>
</div>
