<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Role */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Роли пользователей', 'url' => ['role/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-view padding-md">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-street-view"></i> <?= Html::encode($this->title) ?></h2>

            <p>
                <?= Html::a('Обновить', ['update', 'id' => $model->role_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->role_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'role_id',
                    'name',
                ],
            ]) ?>

        </div>
    </div>
</div>
