<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Role */

$this->title = 'Обновление роли: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Панель управления', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Роли пользователей', 'url' => ['role/index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['role/view', 'id' => $model->role_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-update padding-md">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h2>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
