<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\User */

use backend\assets\theme\gentelella\LoginAsset;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\widgets\Alert;

$this->title = 'Login ' . Yii::$app->id;

LoginAsset::register($this);
?>

<div>
    <div id="wrapper">
        <div id="login" class="animate form">
            <section class="login_content">
                <?php $form = ActiveForm::begin(); ?>
                    <h1><?php echo Yii::t('site/login', 'Login Form'); ?></h1>
                    <?php echo Alert::widget(); ?>
                    <div>
                        <?php echo $form->field($model, 'email')->textInput(['class' => 'form-control text-center', 'placeholder' => $model->getAttributeLabel('email')])->label(false); ?>
                    </div>
                    <div>
                        <?php echo $form->field($model, 'password')->passwordInput(['class' => 'form-control text-center', 'placeholder' => $model->getAttributeLabel('password')])->label(false); ?>
                    </div>
                    <div>
                        <?php echo Html::submitButton(Yii::t('site/login', 'Log in'), ['class' => 'btn btn-default']); ?>
                        <?php echo Html::a(Yii::t('site/login', 'Lost your password?'), Url::to(['site/forgotten']), ['class' => 'reset_pass']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">
                        <div class="clearfix"></div>
                        <div>
                            <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?php echo Yii::$app->id; ?></h1>

                            <p><?php echo Yii::t('global', '&copy;2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template.'); ?></p>
                        </div>
                    </div>
                <?php $form->end(); ?>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
    </div>
</div>
