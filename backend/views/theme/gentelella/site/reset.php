<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\User */

use common\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use backend\assets\theme\gentelella\LoginAsset;

LoginAsset::register($this);

$this->title = Yii::t('site/reset', 'Change Password')
?>
<div>
    <div id="wrapper">
        <div id="login" class="animate form">
            <section class="recovery login_content">
                <?php $form = ActiveForm::begin(); ?>
                <h1><?php echo $this->title; ?></h1>
                <?php echo Alert::widget(); ?>
                <div>
                    <?php echo $form->field($model, 'password')->passwordInput(['class' => 'form-control'])->label(Yii::t('site/reset', 'New password')); ?>
                </div>
                <div>
                    <?php echo $form->field($model, 'confirm')->passwordInput(['class' => 'form-control']); ?>
                </div>
                <div class="text-center">
                    <?php echo Html::submitButton(Yii::t('site/reset', 'Save'), ['class' => 'btn btn-default']); ?>
                    <?php echo Html::a(Yii::t('site/login', 'Log in'), Url::to(['site/login']), ['class' => 'btn btn-link']); ?>
                </div>
                <div class="clearfix"></div>
                <div class="separator">
                    <div class="clearfix"></div>
                    <div>
                        <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?php echo Yii::$app->id; ?></h1>

                        <p><?php echo Yii::t('global', '&copy;2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template.'); ?></p>
                    </div>
                </div>
                <?php $form->end(); ?>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
    </div>
</div>