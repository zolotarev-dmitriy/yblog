<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\User */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\widgets\Alert;
use yii\bootstrap\Html;
use backend\assets\theme\gentelella\LoginAsset;

$this->title = Yii::t('site/forgotten', 'Password Recovery');

LoginAsset::register($this);
?>

<div>
    <div id="wrapper" style="max-width: 400px">
        <div id="login" class="animate form">
            <section class="recovery login_content">
                <?php $form = ActiveForm::begin(); ?>
                <h1><?php echo Yii::t('site/forgotten', 'Password Recovery'); ?></h1>
                <?php echo Alert::widget(); ?>
                <div>
                    <?php echo $form->field($model, 'email')->textInput(['class' => 'form-control', 'placeholder' => $model->getAttributeLabel('email')])->label(false); ?>
                </div>
                <div>
                    <?php echo Html::submitButton(Yii::t('site/forgotten', 'Recover'), ['class' => 'btn btn-default']); ?>
                    <?php echo Html::a(Yii::t('site/login', 'Log in'), Url::to(['site/login']), ['class' => 'reset_pass']); ?>
                </div>
                <div class="clearfix"></div>
                <div class="separator">
                    <div class="clearfix"></div>
                    <div>
                        <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?php echo Yii::$app->id; ?></h1>
                        <p><?php echo Yii::t('global', '&copy;2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template.'); ?></p>
                    </div>
                </div>
                <?php $form->end(); ?>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
    </div>
</div>
