<?php

/* @var $this yii\web\View */

use backend\assets\theme\gentelella\FootAsset;
use backend\assets\theme\gentelella\HeadAsset;

$this->title = Yii::$app->id;
HeadAsset::register($this);
FootAsset::register($this);
?>
<div class="site-index">
</div>
