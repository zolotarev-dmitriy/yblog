<?php
namespace backend\components;

use yii\web\Controller;
use Yii;

/**
 * Class BackendController
 * @package backend\components
 */
abstract class BackendController extends Controller
{
    protected $themePath;

    public function init()
    {
        parent::init();

        $this->themePath = '@backend/views/theme/' . Yii::$app->params['theme'];
        $this->layout = $this->themePath . '/layouts/main';
    }

    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        $view = $this->themePath . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR . $view;

        return parent::render($view, $params);
    }

    /**
     * @param null $layout
     * @return string
     */
    public function getThemePath($layout = null)
    {
        return ($layout) ? $this->themePath . DIRECTORY_SEPARATOR . $layout : $this->themePath;
    }

    /**
     * @param $layout
     */
    protected function setThemeLayout($layout)
    {
        $this->layout = $this->themePath . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . $layout;
    }
}