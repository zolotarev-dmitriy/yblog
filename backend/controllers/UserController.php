<?php

namespace backend\controllers;

use backend\components\BackendController;
use backend\widgets\FileUpload;
use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BackendController
{
    public function init()
    {
        parent::init();

        $this->setThemeLayout('page');
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['index', 'create', 'update', 'delete', 'view'], 'roles' => ['admin']],
                    ['allow' => true, 'actions' => ['profile', 'upload-files', 'change'], 'roles' => ['admin', 'partner']],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'new']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->user_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var User $model */
        $model = $this->findModel($id);
        $model->setScenario('edit');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->user_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionProfile()
    {
        /* @var $model User */
        $model = $this->findModel(Yii::$app->user->identity->getId());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Профиль сохранен успешно!');

            return $this->redirect(['user/profile']);
        } else {
            return $this->render('profile', [
                'model' => $model,
            ]);
        }
    }

    public function actionChange()
    {
        /* @var $user User */
        $user = User::findOne(Yii::$app->user->identity->user_id);
        $user->setScenario('password_change');
        $user->setAttribute('password', '');

        if (Yii::$app->request->getIsPost()) {
            if ($user->load(Yii::$app->request->post()) && $user->validatePassword()) {
                $user->save(false);

                Yii::$app->session->setFlash('success', 'Пароль успешно изменен!');

                return $this->redirect(['change']);
            }
        }

        return $this->render('change', [
            'user' => $user
        ]);
    }

    public function actionUploadFiles()
    {
        FileUpload::uploadFile(['jpg', 'png', 'gif']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
