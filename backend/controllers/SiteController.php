<?php
namespace backend\controllers;

use backend\components\BackendController;
use common\models\User;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'forgotten', 'reset'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionError()
    {
        return $this->render('error');
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionForgotten()
    {
        $model = new User(['scenario' => 'forgotten']);

        if (Yii::$app->user->isGuest) {
            if ($model->load(Yii::$app->request->post()) && $model->validate('email')) {
                if (($user = $model->findByEmail($model->email)) !== null) {
                    $user->setAuthKey();

                    if (Yii::$app->mailer->compose(['html' => 'forgotten-html', 'text' => 'forgotten-text'], ['user' => $user])
                        ->setTo($user->email)
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->id])
                        ->setSubject(Yii::t('site/forgotten', 'Password Recovery'))
                        ->send()) {
                        Yii::$app->session->addFlash('success', Yii::t('site/forgotten', 'E-mail was successfully sent. To recover your password, follow the instructions in\'s e-mail!'));
                    }
                } else {
                    Yii::$app->session->addFlash('error', Yii::t('site/forgotten', 'A user with this email address is not found'));
                }
            }

            $this->setThemeLayout('login');

            return $this->render('forgotten', [
                'model' => $model
            ]);
        } else {
            return $this->redirect(['index']);
        }

    }

    /**
     * @param $auth_key
     * @param $reset_token
     * @return string
     * @throws UserException
     */
    public function actionReset($auth_key, $reset_token)
    {
        if (($model = User::findForReset($auth_key, $reset_token)) !== false) {
            $model->setScenario('reset');

            if ($model->load(Yii::$app->request->post())) {
                    if ($model->password == $model->confirm) {
                        $model->setAttribute('password', md5($model->password));
                        $model->save(false);

                        Yii::$app->session->addFlash('success', Yii::t('site/reset', 'Password changed successfully, you can now log into the control panel'));

                        return $this->redirect(['site/login']);
                    } else {
                        Yii::$app->session->addFlash('error', Yii::t('site/reset', 'Password and Confirm password must be the same'));
                    }
            } elseif (Yii::$app->request->post('User')) {
                Yii::$app->session->addFlash('warning', Yii::t('site/reset', 'Password and Confirm password must be filled'));
            }

            $model->setAttribute('password', '');

            $this->setThemeLayout('login');

            return $this->render('reset', [
                'model' => $model
            ]);
        } else {
            throw new UserException();
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (Yii::$app->user->isGuest) {
            $model = new User(['scenario' => 'auth']);

            if ($model->load(Yii::$app->request->post()) && $model->validate(['email', 'password'])) {
                if ($model->findByLogin(Yii::$app->request->post('User')['email'], Yii::$app->request->post('User')['password'])) {
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('site/login', 'Not correctly entered E-mail and / or password!'));
                    return $this->redirect(['login']);
                }
            }

            $this->setThemeLayout('login');

            return $this->render('login', [
                'model' => $model
            ]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
