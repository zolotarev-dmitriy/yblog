<?php
namespace backend\assets\theme\gentelella;

use yii\web\AssetBundle;
use yii\web\View;

class HeadAsset extends AssetBundle {
    public $sourcePath = '@backend/assets/theme/gentelella/source';

    public $css = [
        //'css/bootstrap.min.css',
        'fonts/css/font-awesome.min.css',
        'css/animate.min.css',
        'css/custom.css',
        'css/icheck/flat/green.css',
        'css/floatexamples.css',
    ];

    public $js = [
        //'js/jquery.min.js',

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
}