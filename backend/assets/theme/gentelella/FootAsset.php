<?php
namespace backend\assets\theme\gentelella;

use yii\web\AssetBundle;
use yii\web\View;

class FootAsset extends AssetBundle {
    public $sourcePath = '@backend/assets/theme/gentelella/source';

    public $css = [
    ];

    public $js = [
        'js/bootstrap.min.js',
        'js/chartjs/chart.min.js',
        'js/nicescroll/jquery.nicescroll.min.js',
        'js/icheck/icheck.min.js',
        'js/custom.js',
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
}