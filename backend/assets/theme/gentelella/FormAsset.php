<?php
namespace backend\assets\theme\gentelella;

use yii\web\AssetBundle;
use yii\web\View;

class FormAsset extends AssetBundle {
    public $sourcePath = '@backend/assets/theme/gentelella/source';

    public $css = [
        'css/icheck/flat/green.css',
        'css/editor/external/google-code-prettify/prettify.css',
        'css/editor/index.css',
        'css/select/select2.min.css',
        'css/switchery/switchery.min.css'
    ];

    public $js = [
        'js/bootstrap.min.js',
        'js/progressbar/bootstrap-progressbar.min.js',
        'js/nicescroll/jquery.nicescroll.min.js',
        'js/icheck/icheck.min.js',
        'js/switchery/switchery.min.js',
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
}