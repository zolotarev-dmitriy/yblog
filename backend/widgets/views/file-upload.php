<?php

use yii\helpers\Html;
use backend\widgets\models\Upload;

/* @var $model \yii\base\Model */
/* @var $thumb string */
/* @var $name string */
/* @var $width string */
/* @var $buttonUpload string */
/* @var $uploadUrl string */
/* @var $options array */

?>
<div class="form-group file-upload" style="width: <?php echo $width; ?>">
    <label class="control-label" for="file-upload-<?php echo $options['id']; ?>"><?php echo $model->getAttributeLabel($name) ?></label>
    <?php if ($thumb !== false) { ?>
    <div class="thumbnail text-center" id="<?php echo $options['id']; ?>-thumb">
        <?php if ($model->$name) { ?>
            <img src="<?php echo ($thumb) ? $thumb : $model->$name; ?>">
        <?php } else { ?>
            <i class="fa fa-image"></i>
        <?php } ?>
    </div>
    <?php } ?>
    <div class="hidden">
        <?php echo Html::input('file', null, null, ['id' => 'file-upload-' . $options['id']]); ?>
    </div>
    <div class="input-group">
        <?php echo Html::input('text', Html::getInputName($model, $name), $model->$name, $options); ?>
        <span class="input-group-btn"><button class="btn btn-default" id="<?php echo $options['id'] ?>-button" type="button"><?php echo $buttonUpload; ?></button></span>
    </div>
</div>

<script type="text/javascript">
    function uploadImage(file) {
        var form = $('<form action="<?php echo $uploadUrl; ?>" method="post" enctype="multipart/form-data"></form>');
        var formData = new FormData(form.get(0));

        formData.append('<?php echo Html::getInputName(new Upload(), 'uploadFile'); ?>', $(file)[0].files[0]);

        $.ajax({
            url: '<?php echo $uploadUrl; ?>',
            type: 'post',
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            beforeSend: function() {
                $(file).attr('disabled', true)
            },
            complete: function() {
                $(file).attr('disabled', false);
            },
            success: function(json) {
                if (json['success'] && json['uploadFile']) {
                    var uploadFile = json['uploadFile'];
                    $('#<?php echo $options['id']; ?>').prop('value', uploadFile);

                    <?php if ($thumb !== false) { ?>
                    if (json['thumb']) {
                        $('#<?php echo $options['id']; ?>-thumb').html('<img src="'+json['thumb']+'">');
                    } else {
                        $('#<?php echo $options['id']; ?>-thumb').html('<?php echo $thumb; ?>');
                    }
                    <?php } ?>

                    <?php echo ($onComplete) ? $onComplete : ''; ?>
                }
            }
        });
    }

    $(function() {
        $('#<?php echo $options['id'] ?>-button').on('click', function() {
            $('#file-upload-<?php echo $options['id']; ?>').trigger('click');
        });


        $('#file-upload-<?php echo $options['id']; ?>').on('change', function() {
            uploadImage(this);
        });


        $('#images-upload').on('submit', function(e){

        });
    })
</script>
