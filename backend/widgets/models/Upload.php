<?php
namespace backend\widgets\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class Upload extends Model
{
    /**
     * @var UploadedFile
     */
    public $uploadFile;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uploadFile'], 'file', 'skipOnEmpty' => false]
        ];
    }

    public function upload()
    {
        if ($this->validate()) {

            $fileDir = '/files/uploads';

            if (!is_dir(Yii::getAlias('@webroot' . $fileDir))) {
                mkdir(Yii::getAlias('@webroot' . $fileDir), 0777);
            }

            $filePrefix = substr(md5(time()), 0, 2);
            $filePath = Yii::getAlias('@webroot' . $fileDir . '/' . $filePrefix);

            $this->uploadFile->saveAs($filePath . '_' . static::transliterate($this->uploadFile->baseName) . '.' . $this->uploadFile->extension);
            $this->uploadFile->name = $fileDir . '/' . $filePrefix . '_' . static::transliterate($this->uploadFile->baseName) . '.' . $this->uploadFile->extension;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $cyrillic
     * @return mixed|string
     */
    public static function transliterate($cyrillic) {
        $a = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',
            'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'ts',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ы' => 'y',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '-' => '_',
            ' ' => '_'
        ];

        $l = mb_strtolower($cyrillic, 'UTF-8');

        $s = '';

        $n = mb_strlen($l);

        for ($i = 0; $i < $n; $i++) {
            $sym = mb_substr($l, $i, 1, 'UTF-8');

            if (isset($a[$sym])) {
                $s .= $a[$sym];
            } else {
                $s .= $sym;
            }
        }

        $s = preg_replace(['~[^\d\w]+~', '~-+~', '~(^-)|(-$)~'], ['-','-',''], $s);

        return $s;
    }
}