<?php
namespace backend\widgets;

use backend\widgets\models\Upload;
use common\components\Image;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\UploadedFile;
use Yii;

class FileUpload extends Widget
{
    const THUMB_WIDTH = 200;
    const THUMB_HEIGHT = 200;

    /**
     * @var Model
     */
    public $model;

    /**
     * @var string
     */
    public $attribute;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;

    /**
     * @var array
     */
    public $options = [
        'class' => 'form-control',
    ];

    /**
     * @var string
     */
    public $buttonUpload = 'Upload';

    /**
     * @var string
     */
    public $uploadUrl = '';

    /**
     * @var string
     */
    public $width = '200px';

    /**
     * @var string
     */
    public $thumb;

    public $multiple = false;

    public $onComplete = false;

    /**
     * @inheritdoc
     * TODO: Сделать проверку на тип файла
     */
    public function init()
    {
        if ($this->name === null && !$this->hasModel()) {
            throw new InvalidConfigException("Either 'name', or 'model' and 'attribute' properties must be specified.");
        }

        if (!$this->uploadUrl) {
            throw new InvalidConfigException("Either 'uploadUrl' property must be specified.");
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->hasModel() ? Html::getInputId($this->model, $this->attribute) : $this->getId();
        }
        parent::init();
    }

    /**
     * @return bool
     */
    protected function hasModel()
    {
        return $this->model instanceof Model && $this->attribute !== null;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('file-upload', [
            'model' => $this->model,
            'name' => $this->name,
            'width' => $this->width,
            'options' => $this->options,
            'buttonUpload' => $this->buttonUpload,
            'uploadUrl' => $this->uploadUrl,
            'thumb' => $this->thumb,
            'onComplete' => $this->onComplete
        ]);
    }

    /**
     * @param array $allowedTypes
     * @param int $thumbWidth
     * @param int $thumbHeight
     */
    public static function uploadFile(array $allowedTypes = [], $thumbWidth = self::THUMB_WIDTH, $thumbHeight = self::THUMB_HEIGHT)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->headers->add('Content-Type', 'text/json');

        $model = new Upload();

        if (Yii::$app->request->getIsPost()) {
            $model->uploadFile = UploadedFile::getInstance($model, 'uploadFile');

            if (in_array($model->uploadFile->extension, $allowedTypes) || !$allowedTypes) {
                if ($model->upload()) {
                    $uploadFile = $model->uploadFile->name;

                    $result = [
                        'success' => true,
                        'uploadFile' => $uploadFile,
                        'thumb' => Image::resize($uploadFile, $thumbWidth, $thumbHeight)
                    ];

                    Yii::$app->response->content = Json::encode($result);
                    Yii::$app->response->send();
                    exit;
                }
            } else {
                $result = [
                    'success' => false,
                    'errors' => [
                        'uploadFile' => ['Данный тип файлов запрещен для загрузки']
                    ]
                ];

                Yii::$app->response->content = Json::encode($result);
                Yii::$app->response->send();
                exit;
            }
        }

        $result = ['success' => false];

        if ($model->hasErrors()) {
            $result['errors'] = $model->getErrors();
        } else {
            $result['errors'] = ['uploadFile' => ['Внутренняя ошибка сервера']];
        }

        Yii::$app->response->content = Json::encode($result);
        Yii::$app->response->send();
        exit;
    }
}