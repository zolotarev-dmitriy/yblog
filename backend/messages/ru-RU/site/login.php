<?php
return [
    'Login Form' => 'Авторизация',
    'Lost your password?' => 'Забыли пароль?',
    'Log in' => 'Войти',
    'Not correctly entered E-mail and / or password!' => 'Не правильно введены E-mail и/или пароль!',
    'Log Out' => 'Выйти',
];