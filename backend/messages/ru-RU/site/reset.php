<?php
return [
    'Change Password' => 'Смена пароля',
    'New password' => 'Новый пароль',
    'Save' => 'Сохранить',
    'Password and Confirm password must be filled' => 'Пароль и повтор пароля должны быть заполнены',
    'Password and Confirm password must be the same' => 'Пароль и повтор пароля должны совпадать',
    'Password changed successfully, you can now log into the control panel' => 'Пароль успешно изменен, теперь вы может войти в панель управления'
];