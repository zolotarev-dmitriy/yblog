<?php
return [
    'Password Recovery' => 'Восстановить пароль',
    'Recover' => 'Восстановить',
    'E-mail was successfully sent. To recover your password, follow the instructions in\'s e-mail!' => 'Е-mail успешно отправлено. Для восстановления пароля следуйте инструкциям в Е-mail.',
    'A user with this email address is not found' => 'Пользователь с таким эмайл-адресом не найлен',
    'Password reset' => 'Cброс пароля',
    'The site <a href="{url}">{host}</a> request was made to reset the password for your account run a network control panel. If this was not you, you should think about the fact that someone is trying to hack into your account.' => 'На сайте <a href="{url}">{host}</a> был сдeлан запрос о сбросе пароля для вашего аккаунта админстратора панели управления. Если это было сделано не вами, то стоит задуматься о том, что кто-то пытается взломать ваш аккаунт.',
    'To reset your password please click <a href="{reset}">here</a>' => 'Для сброса пароля перейдите по <a href="{reset}">этой ссылке</a>',
    'Security' => 'Безопатсность',
    'Turn off the the ability to recover a password in the control panel setting.' => 'Отключите возможность восстановления пароля в настройка панели управления.',
    'Change e-mail' => 'Смените e-mail',
    'Use the email that nobody knows except you' => 'Испольузйте email который ни кто не знает кроме Вас'
];