<?php
return [
    'ID' => 'ID',
    'E-mail' => 'E-mail',
    'Image' => 'Изображение',
    'Password' => 'Пароль',
    'Password confirm' => 'Повтор пароля',
    'Old password' => 'Старый пароль',
    'Role' => 'Роль пользователя',
    'Date added' => 'Дата регистрации',
    'Date modified' => 'Дата изменения',
    'Status' => 'Статус',
    'Username' => 'Имя пользователя',
    'New password and Confirm password must be the same!' => 'Новый пароль и повтор пароля должны совпадать!',
    'New and old passwords should not be the same!' => 'Новый и старый пароли не должны совпадать!',
    'Invalid old password!' => 'Не правильно введен старый пароль!',

];