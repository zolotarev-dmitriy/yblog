<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $user_id
 * @property string $email
 * @property string $password
 * @property string $image
 * @property integer $role_id
 * @property integer $parent_id
 * @property string $created
 * @property string $updated
 * @property integer $status
 * @property string $auth_key
 * @property string $username
 *
 * @property Role $role
 * @property User $parent
 * @property User[] $childs
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;


    public $confirm;
    public $old_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'status'], 'integer'],
            [['created', 'updated', 'image'], 'safe'],
            [['email', 'username'], 'string', 'max' => 128],
            [['password', 'old_password', 'confirm'], 'string', 'max' => 32],
            [['email'], 'unique', 'on' => ['default']],
            [['email'], 'email'],
            [['email'], 'required', 'on' => 'forgotten'],
            [['email', 'password', 'status', 'role_id', 'username'], 'required', 'on' => 'new'],
            [['email', 'status', 'role_id', 'username'], 'required', 'on' => 'edit'],
            [['email', 'password'], 'required', 'on' => ['auth']],
            [['confirm', 'password', 'old_password'], 'required', 'on' => 'password_change'],
            [['confirm', 'password'], 'required', 'on' => 'reset'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('user/user', 'ID'),
            'email' => Yii::t('user/user', 'E-mail'),
            'password' => Yii::t('user/user', 'Password'),
            'image' => Yii::t('user/user', 'Image'),
            'confirm' => Yii::t('user/user', 'Password confirm'),
            'old_password' => Yii::t('user/user', 'Old password'),
            'role_id' => Yii::t('user/user', 'Role'),
            'created' => Yii::t('user/user', 'Date added'),
            'updated' => Yii::t('user/user', 'Date modified'),
            'status' => Yii::t('user/user', 'Status'),
            'username' => Yii::t('user/user', 'Username'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['role_id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['parent_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChilds()
    {
        return $this->hasMany(self::className(), ['user_id' => 'parent_id']);
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'status' => static::STATUS_ENABLED]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getAttribute('user_id');
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @param $email
     * @param $password
     * @param null $role
     * @return bool
     */
    public static function findByLogin($email, $password, $role = null)
    {
        /* @var $result User */
        $result = self::find()->andWhere(['email' => $email, 'password' => md5($password)])->one();

        if ($result && (!$role || $role == $result->role->name)) {
            Yii::$app->user->login($result);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $email
     * @return null|User
     */
    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->scenario == 'new') {
            $this->setAttribute('password', md5($this->getAttribute('password')));
            $this->setAttribute('created', date('Y-m-d H:i:s'));
            $this->setAttribute('parent_id', Yii::$app->user->identity->getId());
        } elseif ($this->scenario == 'edit') {
            $user = (new User())->findOne($this->getPrimaryKey());

            if (md5($this->getAttribute('password')) != $user->password && $this->getAttribute('password') != $user->password) {
                $this->setAttribute('password', md5($this->getAttribute('password')));
            }
        }

        $this->setAttribute('updated', date('Y-m-d H:i:s'));

        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        $result = self::findOne($this->user_id);

        return $result->password;
    }

    /**
     * @return bool
     */
    public function validatePassword()
    {
        $password = $this->getPassword();

        $_password = $this->getAttribute('password');

        if ($password == md5($this->old_password)) {
            if (md5($this->getAttribute('password')) != md5($this->confirm)) {
                $this->addError('confirm', Yii::t('user/user', 'New password and Confirm password must be the same!'));
            } else {
                $this->setAttribute('password', md5($this->getAttribute('password')));
            }

            if ($password == md5($_password)) {
                $this->addError('password', Yii::t('user/user', 'New and old passwords should not be the same!'));
                $this->setAttribute('password', $_password);
            }

        } else {
            $this->addError('old_password', Yii::t('user/user', 'Invalid old password!'));
        }

        return !$this->hasErrors();
    }

    /**
     * @param $string
     * @return string
     */
    public static function generateAuthKey($string)
    {
        return md5($string . Yii::$app->security->generateRandomString());
    }

    /**
     * @return string
     */
    public function getResetToken()
    {
        return md5($this->created.$this->updated.$this->user_id);
    }

    public function setAuthKey()
    {
        $this->setAttribute('auth_key', static::generateAuthKey(time()));
        $this->save(false);
    }

    /**
     * @param $auth_key
     * @param $reset_token
     * @return array|bool|null|\yii\db\ActiveRecord|User
     */
    public static function findForReset($auth_key, $reset_token)
    {
        if (!empty($auth_key) && !empty($reset_token)) {
            $user = self::findBySql("SELECT * FROM `".self::tableName()."` WHERE auth_key = :auth_key AND MD5(CONCAT(created, updated, user_id)) = :reset_token", [':auth_key' => $auth_key, ':reset_token' => $reset_token])->one();

            return ($user) ? $user : false;
        }

        return false;
    }
}
