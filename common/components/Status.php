<?php
namespace common\components;

/**
 * Class Status
 * @package common\components
 */
trait Status
{
    public $status;

    /**
     * @param $status
     * @return string
     */
    public static function getStatus($status)
    {
        return ($status) ? 'Включено' : 'Выключено';
    }
}