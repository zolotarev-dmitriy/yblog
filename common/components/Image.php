<?php
namespace common\components;

use Imagine\Image\Point;
use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Yii;
use yii\base\Component;

class Image extends Component {
    public static function resize($filename, $width, $height, $type = "")
    {
        $base_path = Yii::getAlias('@webroot');

        if (!file_exists($base_path . $filename) || !is_file($base_path . $filename)) {
            return false;
        }

        $info = pathinfo($filename);

        $extension = $info['extension'];

        $old_image = $filename;
        $new_image = '/files/cache/' . self::slugify(mb_substr($filename, 0, mb_strrpos($filename, '.', 'UTF-8'), 'UTF-8') . '-' . $width . 'x' . $height . $type .'.' . $extension);

        if (!file_exists($base_path . $new_image) || (filemtime($base_path . $old_image) > filemtime($base_path . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!file_exists($base_path . $path)) {
                    @mkdir($base_path . $path, 0777, true);
                }
            }

            list($width_orig, $height_orig) = getimagesize($base_path . $old_image);

            if ($height == null) {
                $height = ceil(($width/$width_orig)*$height_orig);
            }

            if ($width_orig != $width || $height_orig != $height) {
                if ($width) {
                    $scaleW = $width_orig/$width;
                } else {
                    $scaleW = 1;
                }

                if ($height) {
                    $scaleH = $height_orig/$height;
                } else {
                    $scaleH = 1;
                }

                $imagine = new Imagine();
                $image = $imagine->open($base_path . $filename);

                if ($width) {
                    $_width = $width * $scaleH;
                } else {
                    $_width = $width_orig;
                    $width = $width_orig;
                }

                if ($height) {
                    $_height = $height * $scaleW;
                } else {
                    $_height = $height_orig;
                    $height = $height_orig;
                }

                if ($scaleH > $scaleW) {
                    $top_x = 0;
                    $top_y = round(($height_orig - $_height) / 2);

                    $image->crop(new Point($top_x, $top_y), new Box($_width, $_height));
                } elseif ($scaleH < $scaleW) {
                    $top_x = round(($width_orig - $_width) / 2);
                    $top_y = 0;
                    $image->crop(new Point($top_x, $top_y), new Box($_width, $_height));
                }

                $image->resize(new Box($width, $height))->save($base_path . $new_image);
            } else {
                copy($base_path . $old_image, $base_path . $new_image);
            }
        }

        return $new_image;
    }

    public static function slugify($string) {
        return strtolower(trim(preg_replace('~[^0-9a-z\.]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }
}