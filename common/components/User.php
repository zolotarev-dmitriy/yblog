<?php
namespace common\components;

use Yii;

/**
 * Class User
 * @package common\components
 * @property \common\models\User $identity
 */
class User extends \yii\web\User
{
    /**
     * @inheritdoc
     */
    public $identityClass = 'common\models\User';

    /**
     * Check if user can do $permissionName.
     * If "authManager" component is set, this will simply use the default functionality.
     * Otherwise, it will use our custom permission system
     *
     * @param string $permissionName
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        /* @var \common\models\User $user */
        $user = $this->getIdentity();

        return (!$this->isGuest && $user->role->name == $permissionName);
    }
}